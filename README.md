# README from original model authors
This UFO file contains an implementation of the Two Higgs Doublet Model (THDM) plus pseudoscalar extension described in arXiv:1701.07427.

The implemented Yukawa sector is of type II. The UFO implementation can be used with MadGraph5_aMC@NLO available at https://launchpad.net/mg5amcnlo.

The additional parameters in the param_card.dat are:

gPXd = The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $`y_\chi`$ in (2.5) of arXiv:1701.07427.

tanbeta	= The ratio of the vacuum expectation values $`\tan \beta = v_2/v_1`$ of the Higgs doublets $`H_2`$ and $`H_1`$, as defined in Section 2.1 of arXiv:1701.07427.

sinbma = The sine of the difference of the mixing angles $`\sin (\beta - \alpha)`$ in the scalar potential containing only the Higgs doublets.  
This quantity is defined in Section 3.1 of arXiv:1701.07427. 

lam3 = The quartic coupling of the scalar doublets $`H_1`$ and $`H_2`$.  
This parameter corresponds to the coefficient $`\lambda_3`$ in (2.1) of arXiv:1701.07427.

lamP1 =	The quartic coupling between the scalar doublets $`H_1`$ and the pseudoscalar $`P`$.  
This parameter corresponds to the coefficient $`\lambda_{P1}`$ in (2.2) of arXiv:1701.07427.

lamP2 =	The quartic coupling between the scalar doublets $`H_2`$ and the pseudoscalar $`P`$.  
This parameter corresponds to the coefficient $`\lambda_{P2}`$ in (2.2) of arXiv:1701.07427.

sinp = The sine of the mixing angle $`\theta`$, as defined in Section 2.1 of arXiv:1701.07427.

Mxd = The mass of the fermionic DM candidate denoted by $`m_\chi`$ in arXiv:1701.07427.

mh1 = The mass of the lightest scalar mass eigenstate $`h`$,  
which is identified in arXiv:1701.07427 with the Higgs-like resonance found
	at the LHC.

mh2 = The mass of the heavy scalar mass eigenstate $`H`$.  
See Section 2.1 of arXiv:1701.07427 for further details.

mh3 = The mass of the heavy pseudoscalar mass eigenstate $`A`$.  
See Section 2.1 of arXiv:1701.07427 for further details.

mhc = The mass of the charged scalar eigenstate $`H^\pm`$.  
See Section 2.1 of arXiv:1701.07427 for further details.

mh4 = The mass of the pseudoscalar mass eigenstate $`a`$ that decouples for $`\sin \theta = 0`$.  
See Section 2.1 of arXiv:1701.07427 for further details.

Wh1 = The total width of the mass eigenstate $`h`$.  
If set to Auto the total and partial decay widths are calculated by MadGraph. 

Wh2 = The total width of the mass eigenstate $`H`$.  
If set to Auto the total and partial decay widths are calculated by MadGraph. 

Wh3 = The total width of the mass eigenstate $`A`$.  
If set to Auto the total and partial decay widths are calculated by MadGraph. 

Whc = The total width of the mass eigenstate $`H^\pm`$.  
If set to Auto the total and partial decay widths are calculated by MadGraph. 

Wh4 = The total width of the mass eigenstate $`a`$.  
If set to Auto the total and partial decay widths are calculated by MadGraph.

In case of questions etc. please write to one of the authors of arXiv:1701.07427:

<bauer@thphys.uni-heidelberg.de>  
<ulrich.haisch@physics.ox.ac.uk>  
<felix.kahlhoefer@desy.de>

# FormFactors Approach
The equations for ggF->A/H vertices for a generic 2HDM are provided by Diogo:

```math
H(p)g^{a,\mu}(p_1)g^{b,\nu}(p_2): \frac{ig_s^2}{12\pi^2v}\left(c_tA_S(\tau_t)+c_bA_S(\tau_b)\right)(p_1^\mu p_2^\nu - p_1 \cdot p_2 g^{\mu\nu})\delta^{ab}
```

```math
A(p)g^{a,\mu}(p_1)g^{b,\nu}(p_2): \frac{ig_s^2}{8\pi^2v}\left(c_t\frac{f(\tau_t)}{\tau_t}+c_b\frac{f(\tau_b)}{\tau_b}\right)\epsilon^{\mu\nu\rho\sigma}p_{1,\rho}p_{2,\sigma}\delta^{ab}
```
where $`c_f`$ is the reduced coupling. $`\tau_f=\frac{p^2}{4m_f^2}`$ and $`\beta=(1-\tau^{-1})^{1/2}`$ is the velocity
of the top quark and top antiquark in the center-of-momentum frame.
```math
A_S(\tau_f) = \frac{3}{2\tau^2}(\tau+(\tau-1)f(\tau))
```

```math
f(\tau)=
\begin{cases}
\arcsin^2(\sqrt\tau),&0 \leq \tau\leq 1\\
-\frac{1}{4}\left[ \log\left(\frac{1+\sqrt{1-\tau^{-1}}}{1-\sqrt{1-\tau^{-1}}}\right) - i\pi \right]^2,& \tau>1
\end{cases}
```

Both can be further divided into two part in __MadGraph__, which are `lorentz structure` and `coupling constant`.
## Coupling Constants
All `coupling constants` are defined in [couplings.py](couplings.py).
### Coupling Constant for H
No changes for H at all compared to Diogo's model.
* Math expression: $`\frac{ig_s^2}{12\pi^2v}`$
* Code implementation:

	```python
	    GC_999 = Coupling(name = 'GC_999',
				    value = '(complex(0,1)*G**2/(12.*cmath.pi**2*vev))',
				    order = {'HIG':1})
	```

### Coupling Constant for a and A
The difference is that the $`a-A`$ mixing angle $`\theta`$ changes the coupling. According to the _yukawa assignments_ in Section 3.2 of arXiv:1701.07427, $`\cos{\theta}`$ is applied to $`A`$ and $`-\sin{\theta}`$ to $`a`$ accordingly.
* Math expression: 

    For $`a`$: $`\sin{\theta}\frac{ig_s^2}{8\pi^2v}`$

    For $`A`$: $`-\cos{\theta}\frac{ig_s^2}{8\pi^2v}`$
* Code implementation:  

    For $`a`$:

    ```python
        GC_997 = Coupling(name = 'GC_997',
    			    value = 'sinp*(complex(0,1)*G**2/(8.*cmath.pi**2*vev))/8.',
    			    order = {'HIG':1})
    ```
    For $`A`$:

    ```python
        GC_998 = Coupling(name = 'GC_998',
    			    value = '-TH3x3*(complex(0,1)*G**2/(8.*cmath.pi**2*vev))/8.',
    			    order = {'HIG':1})
    ```
    , in which `TH3x3` represents $`\cos\theta`$.

### Coupling Order
All ggF->S vertices shares the same `coupling order`, `HIG`, which is defined in [coupling_orders.py](coupling_orders.py), just like the origin model `Higgs_Effective_Couplings_FormFact`.

## Lorentz Structure
All `lorentz structurs` are defined in [lorentz.py](lorentz.py).  
The issue is that the definition of the variables: `ymt`, `ymb` and etc. of the two UFO are different. In short,

```math
c_f = 
\begin{cases}
\frac{y^m_f}{m_f}, & \text{in Diogo's model}\\
\frac{\xi_f y^m_f}{m_f}, & \text{in Uli's model}
\end{cases}
```

i.e.

```math
{y^m_f}^{\mathrm{,Diogo}}={\xi_f y^m_f}^{\mathrm{,Uli}}
```
, where $`\xi_f`$ encode the dependence on the Yukawa type. For example, $`\xi_t=-\cot{\beta}`$ and $`\xi_b=\tan{\beta}`$ in the alignment limit.

### Lorentz Structure for H
* Math expression:

    ```math
    \left(c_tA_S(\tau_t)+c_bA_S(\tau_b)\right)(p_1^\mu p_2^\nu - p_1 \cdot p_2 g^{\mu\nu})
    ```

* Code Implementation

    ```python
        VVS3lo = Lorentz(name = 'VVS3lo',
                         spins = [ 3, 3, 1 ],
                         structure = 'myGGH(2*P(-1,1)*P(-1,2))*(P(1,2)*P(2,1) - P(-1,1)*P(-1,2)*Metric(1,2))')
    ```

    , in which `myGGH` is the form factor, a function of $`s=2{p_{1}}^{\mu}{p_{2}}_{\mu}`$, defined in [Fortran/functions.f](Fortran/functions.f):

    ```fortran
        double complex function myGGH(ss)
        ! ggH coupling form factor
        implicit none     
        include 'input.inc' ! include all model parameter
        INCLUDE 'coupl.inc'
        complex*16 c0
        external c0
        real*8 mb2, mt2,ss
        real*8 ttH, bbH
        
        mt2=mdl_mt**2
        mb2=mdl_mb**2

        ! ymf definitions are not the same as Diogo's model
        ! Here I try to fix it
        ttH=mdl_ymt*(-mdl_sinbma/mdl_tanbeta+dcos(dasin(mdl_sinbma)))
        bbH=mdl_ymb*( mdl_sinbma/mdl_tanbeta+dcos(dasin(mdl_sinbma)))
            
        myGGH = 3d0/ss*(mdl_mt*ttH*(2d0 + (4d0*mt2 - ss)*c0(ss,mt2))
       &        +mdl_mb*bbH*(2d0 + (4d0*mb2 - ss)*c0(ss,mb2)))
        return 
        end
    ```

    , in which `c0` is an external function corresponding to $`f(\tau)`$ defined in the same fortran file:

    ```fortran
        double complex function c0(ss,mass2)
        implicit none

        COMPLEX*16 IMAG1
        PARAMETER (IMAG1=(0D0,1D0))
        real*8 pi,tau,ss,mass2
        parameter (pi=3.141592653589793d0)

        tau=ss/(4d0*mass2)

        if (tau.gt.1d0) then
        c0=1d0/(2d0*ss)*
       &  (log((sqrt(1d0-(1d0/tau))+1d0)/(1d0-sqrt(1d0-(1d0/tau))))
       &  -imag1*pi)**2
        else
        c0=-(2d0/ss)*dasin(sqrt(tau))**2
        endif


        end
    ```

    Note that `ttH` and `bbH` are defined in the way that they reflect the definition difference of `ymt` and `ymb` in the two model.
### Lorentz Structure for a and A
a and A shares the same `lorentz structure`.
* Math Expression:

    ```math
    \left(c_t\frac{f(\tau_t)}{\tau_t}+c_b\frac{f(\tau_b)}{\tau_b}\right)\epsilon^{\mu\nu\rho\sigma}p_{1,\rho}p_{2,\sigma}
    ```

* Code Implementation

    ```python
        VVS1lo = Lorentz(name = 'VVS1lo',
                         spins = [ 3, 3, 1 ],
                         structure = 'myGGA(2*P(-1,2)*P(-1,1))*(-4*Epsilon(1,2,-1,-2)*P(-2,2)*P(-1,1) + 4*Epsilon(1,2,-1,-2)*P(-2,1)*P(-1,2))')
    ```

    , similarly, in which `myGGA` is the form factor, a function of $`s=2{p_{1}}^{\mu}{p_{2}}_{\mu}`$, defined in [Fortran/functions.f](Fortran/functions.f):

    ```fortran
    	double complex function myGGA(ss)
    	! ggA coupling form factor
    	implicit none     
    	include 'input.inc' ! include all model parameter
    	INCLUDE 'coupl.inc'
    	complex*16 c0
    	external c0
    	real*8 mb2, mt2,ss
    	real*8 ttA, bbA

    	mt2=mdl_mt**2
    	mb2=mdl_mb**2

    	! ymf definitions are not the same
    	! Here I try to fix it
    	ttA = mdl_ymt / mdl_tanbeta
    	bbA = mdl_ymb * mdl_tanbeta

    	myGGA = -2d0*(mdl_mb*bbA*c0(ss,mb2)+mdl_mt*ttA*c0(ss,mt2))
     
    	return 
    	end
    ```

    ,in which `c_0` is the same function as described in [Lorentz Structure for H](#lorentz-structure-for-h).
## Validation

### Signal-plus-interference pattern of H
In the decoupling limit of $`a`$: $`\sin\theta=0`$, the UFO degrade to the generic Type-II 2HDM and can be validated against the original model `Higgs_Effective_Couplings_FormFact`.
<img src="./doc/figures/2HDMvs2HDMaH.png" width="640">

### Signal-plus-interference pattern of A
In the decoupling limit of $`a`$: $`\sin\theta=0`$, the UFO degrade to the generic Type-II 2HDM and can be validated against the original model `Higgs_Effective_Couplings_FormFact`.
<img src="./doc/figures/2HDMvs2HDMaA.png" width="640">

### Signal pattern of a
No idea how the Signal-plus-interference pattern of $`a`$ looks. It is only done by comparing the signal pattern to the one in the original loop model `Pseudoscalar_2HDM`.
<img src="./doc/figures/2HDMaa_LoopvsFFac.png" width="640">

## Signal-plus-interference pattern of a+A
It is combined in the sense that $`a\rightarrow t\bar{t}`$ interferes with $`A\rightarrow t\bar{t}`$, and hence in principle not separable. But it seems the interference between them is quite small at least in this case, so probably it can be done separately in the future.
No idea how it would look. Please give it a check if possible:  
### $`m_a=100GeV`$ and $`\sin\theta=1/\sqrt{2}`$
<img src="./doc/figures/2HDMaAa_single.png" width="640">

|                                    Channel | cross section \[pb\] |
|--------------------------------------------|----------------------|
| $`\sigma^\mathcal{S}_A`$                   |      +6.56e-1        |
| $`\sigma^\mathcal{I}_A`$                   |      -3.24e-1        |
| $`\sigma^\mathcal{S}_a`$                   |      +3.12e-3        |
| $`\sigma^\mathcal{I}_a`$                   |      -6.77e-4        |
| $`\sigma^\mathcal{I}_{a\leftrightarrow A}`$|      -1.25e-4        |
| $`\sigma^\mathrm{total}_{A+a}`$            |      +3.16e-1        |

### Vary $`m_a`$ with fixed $`\sin\theta=1/\sqrt{2}`$
<img src="./doc/figures/2HDMaAama.png" width="640">

### Vary $`\sin\theta`$ with fixed $`m_a=100\mathrm{GeV}`$
<img src="./doc/figures/2HDMaAasinp.png" width="640">